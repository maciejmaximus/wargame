﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Classes
{
    public class Card
    {
        public int ID { get; set; }
        public int Value { get; set; }
        public string Type { get; set; }
        public string Sign { get; set; }
    }
}
