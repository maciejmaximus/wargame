﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Classes
{
    public class Player
    {
        public int ID { get; set; }
        public List<Card> PlayerCards { get; set; }
    }
}
