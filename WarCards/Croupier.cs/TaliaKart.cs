﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Classes
{
    public class TaliaKart
    {
        public List<Card> DomyslnaTaliaKart { get; set; }

        public TaliaKart()
        {
            DomyslnaTaliaKart = new List<Card>();
            DomyslnaTaliaKart.Add(new Card { ID = 0, Value = 2, Sign = "2", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 1, Value = 3, Sign = "3", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 2, Value = 4, Sign = "4", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 3, Value = 5, Sign = "5", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 4, Value = 6, Sign = "6", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 5, Value = 7, Sign = "7", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 6, Value = 8, Sign = "8", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 7, Value = 9, Sign = "9", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 8, Value = 10, Sign = "10", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 9, Value = 11, Sign = "J", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 10, Value = 12, Sign = "D", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 11, Value = 13, Sign = "K", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 12, Value = 14, Sign = "A", Type = "♡" });
            DomyslnaTaliaKart.Add(new Card { ID = 13, Value = 2, Sign = "2", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 14, Value = 3, Sign = "3", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 15, Value = 4, Sign = "4", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 16, Value = 5, Sign = "5", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 17, Value = 6, Sign = "6", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 18, Value = 7, Sign = "7", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 19, Value = 8, Sign = "8", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 20, Value = 9, Sign = "9", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 21, Value = 10, Sign = "10", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 22, Value = 11, Sign = "J", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 23, Value = 12, Sign = "D", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 24, Value = 13, Sign = "K", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 25, Value = 14, Sign = "A", Type = "♢" });
            DomyslnaTaliaKart.Add(new Card { ID = 26, Value = 2, Sign = "2", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 27, Value = 3, Sign = "3", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 28, Value = 4, Sign = "4", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 29, Value = 5, Sign = "5", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 30, Value = 6, Sign = "6", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 31, Value = 7, Sign = "7", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 32, Value = 8, Sign = "8", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 33, Value = 9, Sign = "9", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 34, Value = 10, Sign = "10", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 35, Value = 11, Sign = "J", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 36, Value = 12, Sign = "D", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 37, Value = 13, Sign = "K", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 38, Value = 14, Sign = "A", Type = "♠" });
            DomyslnaTaliaKart.Add(new Card { ID = 39, Value = 2, Sign = "2", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 40, Value = 3, Sign = "3", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 41, Value = 4, Sign = "4", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 42, Value = 5, Sign = "5", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 43, Value = 6, Sign = "6", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 44, Value = 7, Sign = "7", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 45, Value = 8, Sign = "8", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 46, Value = 9, Sign = "9", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 47, Value = 10, Sign = "10", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 48, Value = 11, Sign = "J", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 49, Value = 12, Sign = "D", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 50, Value = 13, Sign = "K", Type = "♣" });
            DomyslnaTaliaKart.Add(new Card { ID = 51, Value = 14, Sign = "A", Type = "♣" });
        }
    }
}
