﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Classes
{
    public class Croupier
    {
        public TaliaKart Tasuj ()
        {
            Random random = new Random();
            TaliaKart decks = new TaliaKart();
            int sortMargin = decks.DomyslnaTaliaKart.Count;

            Card[] deckArray = decks.DomyslnaTaliaKart.ToArray();

            for (int i = 0; i < 900; i++)
            {
                for (int j = 0; j < decks.DomyslnaTaliaKart.Count; j++)
                {
                    
                    int randomCardId = random.Next(0, sortMargin);
                    Card temporaryCard = deckArray[randomCardId];
                    deckArray[randomCardId] = deckArray[j];
                    deckArray[j] = temporaryCard;

                    if (sortMargin == 0)
                    {
                        sortMargin = 0;
                    }
                    else
                    {
                        sortMargin--;
                    }
                }
            }
            decks.DomyslnaTaliaKart = deckArray.ToList();
            return decks;
        }

        public Card Rozdaj(TaliaKart talia)
        {
            Random random = new Random();
            int cardID = random.Next(0, talia.DomyslnaTaliaKart.Count);

            return talia.DomyslnaTaliaKart[cardID];
        }
    }
}
