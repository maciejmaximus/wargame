﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Classes;

namespace WarCards
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<Card> player1Cards = new List<Card>();
        List<Card> player2Cards = new List<Card>();
        List<Card> cardsOnTable = new List<Card>();
        int btnPlayClickCounter = 0;

        private void btnStart_Click(object sender, EventArgs e)
        {
            player1Cards.Clear();
            player2Cards.Clear();
            cardsOnTable.Clear();
            lbTable.Items.Clear();

            btnPlayClickCounter = 0; // do przycisku Play

            TaliaKart talia = new TaliaKart();
            int cardsAmount = talia.DomyslnaTaliaKart.Count;

            Croupier croupier = new Croupier();
            TaliaKart cardsToGive = croupier.Tasuj();

            for (int i = 0; i < cardsAmount / 2; i++)
            {
                Card chosedCardForPlayer1 = croupier.Rozdaj(cardsToGive);
                player1Cards.Add(chosedCardForPlayer1);
                cardsToGive.DomyslnaTaliaKart.Remove(chosedCardForPlayer1);
                Card chosedCardForPlayer2 = croupier.Rozdaj(cardsToGive);
                player2Cards.Add(chosedCardForPlayer2);
                cardsToGive.DomyslnaTaliaKart.Remove(chosedCardForPlayer2);
            }

            lbPlayer1.Items.Clear();
            lbPlayer2.Items.Clear();
            foreach (Card card in player1Cards)
            {
                lbPlayer1.Items.Add(card.Sign + " " + card.Type);
            }
            foreach (Card card in player2Cards)
            {
                lbPlayer2.Items.Add(card.Sign + " " + card.Type);
            }

            //TaliaKart talia = new TaliaKart();
            //foreach (Card card in talia.DomyslnaTaliaKart)
            //{
            //    lbPlayer1.Items.Add(card.Sign + " " + card.Type);
            //}
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            int lastItemOnTableIndex = lbTable.Items.Count - 1;

            


                if (btnPlayClickCounter % 2 == 0)
                {
                    lbTable.Items.Add(lbPlayer1.Items[0]);
                    lbTable.Items.Add(lbPlayer2.Items[0]);

                    lbPlayer1.Items.Remove(lbPlayer1.Items[0]);
                    lbPlayer2.Items.Remove(lbPlayer2.Items[0]);

                    cardsOnTable.Add(player1Cards[0]);
                    cardsOnTable.Add(player2Cards[0]);

                    player1Cards.Remove(player1Cards[0]);
                    player2Cards.Remove(player2Cards[0]);


                }
                else
                {
                    if (cardsOnTable[lastItemOnTableIndex - 1].Value > cardsOnTable[lastItemOnTableIndex].Value)
                    {
                        cardsOnTable.Reverse();
                        foreach (Card card in cardsOnTable)
                        {
                            player1Cards.Add(card);
                            lbPlayer1.Items.Add(card.Sign + " " + card.Type);
                        }
                        cardsOnTable.Clear();
                        lbTable.Items.Clear();
                    }
                    else if (cardsOnTable[lastItemOnTableIndex - 1].Value < cardsOnTable[lastItemOnTableIndex].Value)
                    {
                        cardsOnTable.Reverse();
                        foreach (Card card in cardsOnTable)
                        {
                            player2Cards.Add(card);
                            lbPlayer2.Items.Add(card.Sign + " " + card.Type);
                        }
                        cardsOnTable.Clear();
                        lbTable.Items.Clear();
                    }
                    else
                    {
                    }
                }

                btnPlayClickCounter++;
        }
    }
}
