﻿namespace WarCards
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbPlayer1 = new System.Windows.Forms.ListBox();
            this.Player1 = new System.Windows.Forms.Label();
            this.Player2 = new System.Windows.Forms.Label();
            this.lbPlayer2 = new System.Windows.Forms.ListBox();
            this.War = new System.Windows.Forms.Label();
            this.lbTable = new System.Windows.Forms.ListBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbPlayer1
            // 
            this.lbPlayer1.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbPlayer1.FormattingEnabled = true;
            this.lbPlayer1.ItemHeight = 15;
            this.lbPlayer1.Location = new System.Drawing.Point(25, 25);
            this.lbPlayer1.Name = "lbPlayer1";
            this.lbPlayer1.Size = new System.Drawing.Size(127, 544);
            this.lbPlayer1.TabIndex = 0;
            // 
            // Player1
            // 
            this.Player1.AutoSize = true;
            this.Player1.Location = new System.Drawing.Point(65, 9);
            this.Player1.Name = "Player1";
            this.Player1.Size = new System.Drawing.Size(42, 13);
            this.Player1.TabIndex = 1;
            this.Player1.Text = "Player1";
            // 
            // Player2
            // 
            this.Player2.AutoSize = true;
            this.Player2.Location = new System.Drawing.Point(407, 9);
            this.Player2.Name = "Player2";
            this.Player2.Size = new System.Drawing.Size(42, 13);
            this.Player2.TabIndex = 2;
            this.Player2.Text = "Player2";
            // 
            // lbPlayer2
            // 
            this.lbPlayer2.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbPlayer2.FormattingEnabled = true;
            this.lbPlayer2.ItemHeight = 15;
            this.lbPlayer2.Location = new System.Drawing.Point(360, 25);
            this.lbPlayer2.Name = "lbPlayer2";
            this.lbPlayer2.Size = new System.Drawing.Size(134, 544);
            this.lbPlayer2.TabIndex = 3;
            // 
            // War
            // 
            this.War.AutoSize = true;
            this.War.Location = new System.Drawing.Point(238, 109);
            this.War.Name = "War";
            this.War.Size = new System.Drawing.Size(34, 13);
            this.War.TabIndex = 4;
            this.War.Text = "Table";
            // 
            // lbTable
            // 
            this.lbTable.Font = new System.Drawing.Font("Arial Unicode MS", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbTable.FormattingEnabled = true;
            this.lbTable.ItemHeight = 15;
            this.lbTable.Location = new System.Drawing.Point(196, 125);
            this.lbTable.Name = "lbTable";
            this.lbTable.Size = new System.Drawing.Size(120, 184);
            this.lbTable.TabIndex = 5;
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(196, 39);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(120, 41);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(196, 326);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(120, 35);
            this.btnPlay.TabIndex = 7;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 580);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lbTable);
            this.Controls.Add(this.War);
            this.Controls.Add(this.lbPlayer2);
            this.Controls.Add(this.Player2);
            this.Controls.Add(this.Player1);
            this.Controls.Add(this.lbPlayer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbPlayer1;
        private System.Windows.Forms.Label Player1;
        private System.Windows.Forms.Label Player2;
        private System.Windows.Forms.ListBox lbPlayer2;
        private System.Windows.Forms.Label War;
        private System.Windows.Forms.ListBox lbTable;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnPlay;
    }
}

